/*
 Написать программу ”Угадай число”. Программа задумывает число в диапа-
зоне от 1 до 100 и пользователь должен угадать его за наименьшее количество
попыток.
Замечание:
Пользователь вводит число, а программа подсказывает ему: ”больше”, ”мень-
ше”, ”угадал!”.
*/

#include <stdio.h>
#include <stdlib.h>
#include <time.h>

int main()
{
    unsigned int n;
    unsigned int inputn = 0;
    srand(time(NULL));
    
    n = rand() % 99 + 1;
    
    //printf("%u\n", n);
    
    while(inputn != n)
    {
        printf("Input your choice: ");
        scanf("%u", &inputn);
 
        if(inputn > n)
            printf("\nYour choice bigger than N\n");
        else if(inputn < n)
            printf("\nYour choice less than N\n");
    }
    printf("YOU WIN!!!\n");
    return 0;
}