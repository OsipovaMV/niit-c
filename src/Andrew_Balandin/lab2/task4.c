/*
Написать программу, переставляющую символы в массиве согласно правилу:
сначала идут латинские буквы, потом цифры. Строка задается в коде програм-
мы в виде случайной последовательности букв и цифр. Пользоваться дополни-
тельными массивами нельзя.
Замечание:
Сортировка в данной задаче неприменима ввиду ее трудоемкости. Нужно ис-
пользовать группировку элементов массива.
*/


#include <stdio.h>
#include <time.h>
#include <stdlib.h>
#include <ctype.h>

#define N_DIGITS_LETTERS ('z' - 'a' + 1 + 'Z' - 'A' + 1 + '9' - '0' + 1)
#define N_ARR_ELEMENTS 50

void printArr(char arr[], int n)
{
    int i;
    
    for(i = 0; i < n; i++)
        printf("%c ", arr[i]);
    printf("\n");
}

void fillArr(char arr[], int n)
{
    int i, m;
    
    char alph[N_DIGITS_LETTERS] = {0};
    
    for(i = 0; i <= 'z' - 'a'; i++)
        alph[i] = 'a' + i;
    m = i;
    for(i = 0; i <= 'Z' - 'A'; i++)
        alph[i + m] = 'A' + i;
    m += i;
    for(i = 0; i <= '9' - '0'; i++)
        alph[i + m] = '0' + i;
    //printArr(alph, N_DIGITS_LETTERS);
    
    srand(time(NULL));

    for(i = 0; i < n; i++)
        arr[i] = alph[rand() % N_DIGITS_LETTERS];

}

void letnumArr(char arr[], int n)
{
    int i = 0, j = n - 1;
    //char tmp;

    while(i < j)
        if(isdigit(arr[i]))
            if(isalpha(arr[j]))
            {
                arr[i] += arr[j];
                arr[j] = arr[i] - arr[j];
                arr[i] = arr[i] - arr[j];
                i++; 
                j--;
            }
            else
                j--;
        else
            i++;
}

int main()
{
    char arr[N_ARR_ELEMENTS] = {0};
    
    fillArr(arr, N_ARR_ELEMENTS);
    printArr(arr, N_ARR_ELEMENTS);
    
    printf("\n\n\n");
    
    letnumArr(arr, N_ARR_ELEMENTS);
    printArr(arr, N_ARR_ELEMENTS);
    
    return 0;
}