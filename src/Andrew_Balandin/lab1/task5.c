/*
Написать программу, которая принимает строку от пользователя и
выводит её на экран, выравнивая по центру
 */

#include <stdio.h>
#include <string.h>

#define WIDTH_OF_TERMINAL 150

int main()
{
    char    input_str[WIDTH_OF_TERMINAL] = {0},
            format[10] = {0};
    
    printf("Input string: ");
    fgets(input_str, WIDTH_OF_TERMINAL, stdin);
    printf("\n\n\n\n\n");
    sprintf(format, "%%%d.%lus", WIDTH_OF_TERMINAL / 2, strlen(input_str) / 2);
    printf(format, input_str);
    printf("%s", input_str + strlen(input_str) / 2);
    printf("\n\n\n\n\n");

    return 0;
}