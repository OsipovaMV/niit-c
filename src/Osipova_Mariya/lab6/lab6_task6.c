#include <stdio.h>
#include <math.h>
#include <time.h>
#include <string.h>
#include <windows.h>


struct FIBret
{
	int sum;
	int sumBefore;      //���������� ����� �������� � ����
};

struct FIBret fibRec(int N)
{
	struct FIBret fib;
	if (N>2)
	{
		struct FIBret fibBefore = fibRec(N - 1);
		fib.sum = fibBefore.sumBefore + fibBefore.sum;
		fib.sumBefore = fibBefore.sum;
		return fib;
	}
	else
	{
		fib.sum = 1;
		fib.sumBefore = 1;
		return fib;
	}
}

int fibonachi(int N)
{
	struct FIBret fr = fibRec(N);
	return fr.sum;
}

int main()
{
	SetConsoleCP(1251); //��������� ������� �������� win-cp 1251 � ����� �����
	SetConsoleOutputCP(1251); //��������� ������� �������� win-cp 1251 � ����� ������ 
	int N, i, fib;
	clock_t timeStart, timeEnd;
	FILE *fp;
	double summ;
	N = 40;

	fp = fopen("tabl.txt", "wt");
	for (i = 2; i <= N; i++)
	{
		timeStart = clock();
		fib = fibonachi(i);
		timeEnd = clock();
		fprintf(fp, "%d\t%f\n", i, (float)(timeEnd - timeStart) / CLOCKS_PER_SEC);
		printf("%d�������� \t����� %d \t\t����� ���������� %f sec\n", i, fib, (float)(timeEnd - timeStart) / CLOCKS_PER_SEC);
	}
	fclose(fp);
}