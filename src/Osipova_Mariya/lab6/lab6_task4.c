/*�������� ���������, ������� ��������� ������ ������������ �
����������� ���������*/
#include <stdio.h>
#include <math.h>
#include <time.h>
#include <string.h>
#include <windows.h>

double summSimple(double * arr, int N)/*������������ ������� ��������*/
{
	int i;
	double summ = 0;

	for (i = 0; i<N; i++)
	{
		summ += arr[i];
	}
	return summ;
}

double summRec(double * arr, int N)/*������������ ����������� ��������*/
{

	if (N>1)
	{
		return summRec(arr, N / 2) + summRec(arr + N / 2, N / 2);
	}
	else
	{
		return arr[0];
	}
}

int main(int argc, char*argv[])
{
	SetConsoleCP(1251); //��������� ������� �������� win-cp 1251 � ����� �����
	SetConsoleOutputCP(1251); //��������� ������� �������� win-cp 1251 � ����� ������ 
	int M, N, i;
	clock_t timeStart, timeEnd;
	double *arr;
	double summ;

	printf("������� ������� ������\n");
	scanf("%d", &M);
	N = pow(2.0, M);
	printf("���������� ��������� = %d\n", N);
	arr = (double*)calloc(N, sizeof(double));
	srand(time(0));
	for (i = 0; i<N; i++)
	{
		arr[i] = (double)(rand() % 1000) / 1000;
	}

	timeStart = clock();
	summ = summSimple(arr, N);
	timeEnd = clock();
	printf("������������ ������� �������� = %f, ����� ������ = %f\n", (float)summ, (float)(timeEnd - timeStart) / CLOCKS_PER_SEC);

	timeStart = clock();
	summ = summRec(arr, N);
	timeEnd = clock();
	printf("������������ ����������� ��������= %f, ����� ������ = %f\n", (float)summ, (float)(timeEnd - timeStart) / CLOCKS_PER_SEC);

	free(arr);
	return 0;
}