/*�������� ���������, ������� ������� � ��������� ����� ����� ��
2 �� 1000000 �����, ����������� ����� ������� �������������-
����� ��������*/

#include <stdio.h>
#include <windows.h>

long long int Collatz(long long int n, long long int * i)
{
	(*i)++;
	if (n == 1)
		return n;
	if (n % 2)
		return Collatz(3 * n + 1, i);
	else
		return Collatz(n / 2, i);
}

int main()
{
	SetConsoleCP(1251); //��������� ������� �������� win-cp 1251 � ����� �����
	SetConsoleOutputCP(1251); //��������� ������� �������� win-cp 1251 � ����� ������ 
	long long int posl = 0, i, max = 0, n = 2;

	for (i = 2; i <= 1000000; i++)
	{
		posl = 0;
		Collatz(i, &posl);
		if (posl > max)
		{
			max = posl;
			n = i;
		}
	}
	printf("������������ ����� ��������� = %lld, ��� n = %lld \n", max, n);
	return 0;
}

