/*�������� ���������, ������� ��������� � ��������� ����������
������� ����������� ����������� � ������� ��� �� ������� (��.
�������)*/

#define _CRT_SECURE_NO_WARNING
#include<stdio.h>
#define MIN_SIZE 3 /*����������� ������*/
#define SCALE 3 /*�������*/
#define M 30
#define N 30
char arr[M][N] = {0};

void print_cross(int size, int x, int y) /*��������� ������ �������� � x,y - ���������� ������*/
{
	int i, j;
	for (i = x - (size / 2)-1; i < x + (size / 2); i++)
	{
		arr[i][y-1] = '*';
	}
	for (j = y - (size / 2)-1; j < y + (size / 2); j++)
	{
		arr[x-1][j] = '*';
	}
}

void fractal(int size, int x, int y)/*��������� ������ �������� � x,y - ���������� ������*/
{
	if (size < MIN_SIZE)
	{ 
		return; 
	}
	
	print_cross(size, x, y);
	size= size / SCALE;
	print_cross(size, x - size, y);
	print_cross(size, x + size, y);
	print_cross(size, x , y- size);
	print_cross(size, x , y+ size);

	fractal(size, x, y);
	fractal(size, x - size, y);
	fractal(size, x + size, y);
	fractal(size, x, y - size);
	fractal(size, x, y + size);
}

void print_arr(void)
{
	int i, j;
	for (j = 0; j < N; j++)
	{
		for (i = 0; i < M; i++)
		{
			if (arr[i][j] == 0)
			{
				putchar(' ');
			}
			else
			{
				putchar('*');
			}
		}
		putchar('\n');
	}
}

int main()
{
	fractal(27, M / 2, N / 2);
	print_arr();
	return 0;
}