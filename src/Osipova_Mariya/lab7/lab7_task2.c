/*�������� ���������, ������� ����������� �������� ��� ����� ��
����� �� � ������� ������� ������������� �������� ���� �����.
�������� ����� �������� � ��������� �����.*/
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <windows.h>

struct Item
{
	char word[10];
	int count;
	struct Item *left;
	struct Item *right;
};

typedef struct Item Item;

char buf[15];
char * chomp(char * str);
Item * makeTree(char * fileName);
Item * searchTree(Item *root, char *str);
void printTree(Item * tree); 
Item * makeItem(Item *root, char *add);


int main(int argc, char*argv[])
{
	SetConsoleCP(1251); //��������� ������� �������� win-cp 1251 � ����� �����
	SetConsoleOutputCP(1251); //��������� ������� �������� win-cp 1251 � ����� ������ 
	FILE * codeFile;
	int i = 0, inWord = 0;
	char ch;
	Item *tree = makeTree(argv[1]);
	if (!tree) 
	{ 
		return 1; 
	}

	codeFile = fopen(argv[2], "rt");
	while ((ch = fgetc(codeFile)) != -1)
	{
		if (ch >= 'a' && ch <= 'z')
		{
			if (inWord == 0)
			{
				i = 0;
				buf[i] = ch;
				inWord = 1;
			}
			else
			{
				i++;
				buf[i] = ch;
			}
		}
		else
		{
			if (inWord == 1)
			{
				i++;
				buf[i] = 0;
				i = 0;
				inWord = 0;
				searchTree(tree, buf);
			}
		}
	}
	fclose(codeFile);
	printTree(tree);
	return 0;
}

Item * makeTree(char * fileName)
{
	FILE *fileIN;
	Item *tree;

	fileIN = fopen(fileName, "rt");
	if (!fileIN)
	{
		printf("������! ���� �� ������ %s", fileIN);
		return NULL;
	}

	tree = NULL;
	while (fgets(buf, 15, fileIN))
	{
		tree = makeItem(tree, chomp(buf));
	}

	fclose(fileIN);
	return tree;
}

Item * makeItem(Item *root, char *add)
{
	if (add[0] == 0) { return NULL; }

	if (root == NULL)
	{
		root = (Item*)malloc(sizeof(Item));
		strcpy(root->word, add);
		root->count = 0;
		root->left = NULL;
		root->right = NULL;
	}
	else if (strcmp(root->word, add)>0)
		root->left = makeItem(root->left, add);
	else if (strcmp(root->word, add)<0)
		root->right = makeItem(root->right, add);
	return root;
}

char * chomp(char * str)
{
	if (str[strlen(str) - 1] == '\n')
		str[strlen(str) - 1] = 0;
	return str;
}

void printTree(Item * tree)
{
	if (tree)
	{
		printTree(tree->left);
		printf("%s:\t%d raz(a)\n", tree->word, tree->count);
		printTree(tree->right);
	}

}

Item * searchTree(Item *root, char *str)
{
	if (root == NULL)
	{
		return NULL;
	}
	if (strcmp(root->word, str) > 0)
		root->left = searchTree(root->left, str);
	else if (strcmp(root->word, str) < 0)
		root->right = searchTree(root->right, str);
	else
		root->count++;
	return root;
}