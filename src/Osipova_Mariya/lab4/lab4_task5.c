/*�������� ���������, ������� ��������� ������������ ������ ������� -
�� ����� � ����������, � ����� ��������� �� � ������� �������� -
��� ����� ������*/

#include <stdio.h>
#include <windows.h>

#define _CRT_SECURE_NO_WARNINGS
#define N 20 /*������������ ���������� �����*/
#define DL 80 /*������������ ���������� �������� � ������*/

//����� ��������� ���������
int compare(const void *a, const void *b)
{
	if (strlen(*(char**)a) > strlen(*(char**)b)) //(char**)a - ���������� � � ���� char**
		return 1;
	else
		return -1;
}

int main()
{
	SetConsoleCP(1251); //��������� ������� �������� win-cp 1251 � ����� �����
	SetConsoleOutputCP(1251); //��������� ������� �������� win-cp 1251 � ����� ������ 

	int kol = 0; /*���������� ��������� �����*/
	char massiv[N][DL];
	char * p[N]; /*������ ����������*/
	int i;
	FILE *fp;
	char fileName[80];

	/*�������� ����� ��� ������*/
	while (1)
	{
		printf("������� ��� �����: ");
		fgets(fileName, 80, stdin);
		if (fileName[0] == '\n')
		{
			printf("�������� �������� �������������.\n");
			return 1;
		}
		fileName[strlen(fileName) - 1] = 0;
		fp = fopen(fileName, "rt"); /*�������� ���������� ����� ��� ������ � ������*/
		if (fp)
		{
			break;
		}
		printf("������! �� ���� �������: %s)\n", fileName);
	}

	printf("\n--------------------------------File-------------------------------------------\n");


	for (i = 0; i < N && fgets(&massiv[i][0], DL, fp); i++)
	{
		printf("%s", massiv[i]);
		if (massiv[i][0] == '\n')
		{
			continue;
		}
		else
		{
			kol++;
			p[kol - 1] = &massiv[i][0];
		}
	}
	qsort(p, kol, sizeof(p[0]), compare); /*���������� �����*/
	/*����� ���o� � ����*/
	for (i = 0; i < kol; i++)
	{
		fputs(p[i], fp);
		printf("%d - %s\n", i + 1, p[i]);
	}
	fclose(fp);
	return 0;
}
