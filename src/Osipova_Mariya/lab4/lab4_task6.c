/*�������� ���������, ������� ����������� ���������� ������������� � �����,
� ����� ��������� ������ ��� ������������ � ��� �������. ��������� ������
���������� ������ �������� � ������ ������� ������������*/
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <windows.h>

#define _CRT_SECURE_NO_WARNINGS
#define N 20 /*������������ ���������� �������������*/
#define DL 80 /*������������ ����� �����*/

int main()
{
	SetConsoleCP(1251); //��������� ������� �������� win-cp 1251 � ����� �����
	SetConsoleOutputCP(1251); //��������� ������� �������� win-cp 1251 � ����� ������ 

	int youngAge = 200; /*������� ������ �������*/
	int oldAge = 0; /*������� ������ ��������*/
	char massiv[N][DL];/*������ ��� �������� �������� ������*/
	char buf[DL];/*����� ��� �������� ������������� ������*/
	char * young; /*��������� �� ������ ������ ��� ��������� ��� ������ �������� ������������*/
	char * old; /*��������� �� ������ ������ ��� ��������� ��� ������ ������� ������������*/
	int kol;/*���������� �������������*/ 
	int age; /*������� ���������������� ������������*/
	int i;/*�������*/

	printf("������� ���������� �������������: ");
	fgets(buf, DL, stdin);
	kol = atoi(buf);

	for (i = 0; i < kol; i++)
	{
		printf("������� ��� %d ������������: ", i + 1);
		fgets(massiv[i], DL, stdin);

		printf("������� ������� %d ������������: ", i + 1);
		fgets(buf, 4, stdin);
		age = atoi(buf);

		if (age > oldAge)
		{
			old = massiv[i];
			oldAge = age;
		}
		if (age < youngAge)
		{
			young = massiv[i];
			youngAge = age;
		}
	}

	printf("\n����� ������� �����������: %s��� %d ���(����)\n", young, youngAge);
	printf("����� ������ �����������: %s��� %d ���(����)\n", old, oldAge);
	return 0;
}