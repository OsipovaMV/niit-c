#include <stdio.h>
#include <time.h>
#include <stdlib.h>
#include <windows.h>

void printWord(char * pWord);//������� ����� �� ������ (�� ����� ������ ��� �������)
int getWords(char * str, char * pWords[]); //��������� ������ ���������� �������� ������ ���� ����
void printWordsRandom(int kol, char * pWords[]);//������� ����� �� ������ � ��������� �������

void printWord(char * pWord)
{
	int nom;//����� �����
	for (nom = 0; *(pWord + nom) != ' '&&*(pWord + nom) != '\n'&&*(pWord + nom) != 0; nom++)
	{
		printf("%c", *(pWord + nom));
	}
	printf(" ");
}

int getWords(char * str, char * pWords[])
{
	int i, cout = 0;
	int inWord = 0;//������� ���������
	
	for (i = 0; str[i] && str[i] != '\n'; i++)
	{
		if (inWord == 0 && str[i] != ' ')
		{
			inWord = 1;
			cout++;
			pWords[cout - 1] = &str[i];
		}
		if (str[i] == ' ') { inWord = 0; }
	}
	return cout;
}

void printWordsRandom(int cout, char * pWords[])
{
	char * pWordsLoc[80];
	int i;

	for (i = 0; i<cout; i++)
	{
		pWordsLoc[i] = pWords[i];
	}

	for (; cout>0; cout--)
	{
		i = rand() % cout;
		printWord(pWordsLoc[i]);
		pWordsLoc[i] = pWordsLoc[cout - 1];
	}
}