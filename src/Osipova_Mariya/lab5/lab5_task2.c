#include <stdio.h>
#include <time.h>
#include <windows.h>

#define _CRT_SECURE_NO_WARNINGS
#define N 24 /*������������ ���������� �������������*/
#define M 80 /*������������ ����� �����*/

void clearBuf(char(*scrBuf)[N][M]);
void createBase(int starsCout, char(*scrBuf)[N][M]);
void createKaleydoskope(char(*scrBuf)[N][M]);
void drawKaleydoskope(char(*scrBuf)[N][M]);
void timeStop(int mSek);

int main()
{
	char screenBuf[N][M];/*��������� ������ ���� char, ������������ ������� ������������ */

	while (1)
	{
		clearBuf(screenBuf);/*�������, ����������� ������� �������*/
		createBase(100, screenBuf);/*�������, ����������� ������������ ��������� ������� �������� ������ ���������*/
		createKaleydoskope(screenBuf);/*�������, ����������� ������������ ������������*/
		system("cls");/*������� ������*/
		drawKaleydoskope(screenBuf);/*�������, ����������� ����� ������������ �� �����*/
		timeStop(500);/*��������� ��������*/
	}

}

void clearBuf(char Buf[N][M])/*�������, ����������� ������� �������*/
{
	int x, y;
	for (y = 0; y<N; y++)
	{
		for (x = 0; x<M; x++)
		{
			Buf[y][x] = ' ';
		}
	}
}

void createBase(int starsCount, char Buf[N][M])/*�������, ����������� ������������ ��������� ������� �������� ������ ���������*/
{
	int i;
	for (i = 0; i<starsCount; i++)/*starsCount - ���������� ��������� � ������� ����� ����� */
	{
		Buf[rand() % N][rand() % M] = '*';
	}
}

void createKaleydoskope(char Buf[N][M])/*�������, ����������� ������������ ������������*/
{
	int x, y;
	for (y = 0; y < (N / 2); y++)
	{
		for (x = 0; x < (M / 2); x++)
		{
			Buf[y][(M - 1) - x] = Buf[y][x];/*����������� �������� � ������ ������� �� ������ �������� ���������*/
			Buf[(N - 1) - y][(M - 1) - x] = Buf[y][x];/*����������� �������� � ������ ������ �� ������ �������� ���������*/
			Buf[(N - 1) - y][x] = Buf[y][x];/*����������� �������� � ����� ������ �� ������ �������� ���������*/
		}
	}
}

void drawKaleydoskope(char Buf[N][M])/*�������, ����������� ����� ������������ �� �����*/
{
	int x, y;
	for (y = 0; y<N; y++)
	{
		for (x = 0; x<M; x++)
		{
			printf("%c", Buf[y][x]);
		}
	}
}

void timeStop(int mSek)/*��������� ��������*/
{
	Sleep(mSek);
}