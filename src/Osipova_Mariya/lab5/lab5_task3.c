/*�������� ���������, �������������� ��������� ������� ����� -
�� ������� ����� ������ ������ ���������� �����, ����� �������
� ����������, �� ���� ������ � ����� ����� �������� �� ������.
���������:
��������� ��������� ������������ ���������� ���� � ������ ��� ������� -
��.��� ������ ������ ����������� �������� �� ����� � ����������� ���� -
����� ������� �����.*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <windows.h>
#include <errno.h>

#define _CRT_SECURE_NO_WARNINGS

#define N 20 /*������������ ���������� �����*/
#define DL 80/*������������ ���������� �������� � ������*/

int openFile(char strArr[][DL]);
int getWords(char str[], char *wordsStart[], char *wordsEnd[]);
void changeWord(char *wordStart, char *wordEnd);


int main()
{
	SetConsoleCP(1251); //��������� ������� �������� win-cp 1251 � ����� �����
	SetConsoleOutputCP(1251); //��������� ������� �������� win-cp 1251 � ����� ������ 
	int kolStrok, kolSlov, i, n;
	char str[N][DL];            //������ �����
	char * wordsStart[DL / 2];    //������ ���������� �� ������ ����
	char * wordsEnd[DL / 2];      //������ ���������� �� ����� ����

	kolStrok = openFile(str);/*����������� ���������� �����*/
	if (kolStrok == -1)
	{
		printf("�������� ���������� �������������.\n");
		return 1;
	}
	/*_______________________________________________________________________*/

	printf("\n\n��������� ����...");
	for (i = 0; i<kolStrok; i++)
	{
		kolSlov = getWords(str[i], wordsStart, wordsEnd);   //��������� ������� ���������� �� ������ � ����� ���� � ������
		for (n = 0; n<kolSlov; n++)
		{
			changeWord(wordsStart[n], wordsEnd[n]);         //������������ �������� � n-�� �����
		}
	}

	printf("��������� ���������.\n");
	printf("\n--------------------------------New text---------------------------------------\n");
	for (i = 0; i<kolStrok; i++)
	{
		printf("%s", str[i]);
	}
	printf("\n--------------------------------New text---------------------------------------\n");

	return 0;
}

int openFile(char strArr[N][DL]) /*������� �������� �����*/
{
	int i = 0;/*�������*/
	int kol = 0;/*���������� �����*/
	FILE *fin;
	char fileName[80];/*������ ��� �������� �������� ������*/

	while (1)/*��������� ����� ����� � ��� ��������*/
	{
		/*printf("������� ��� �����: ");*/
		/*fgets(fileName, 80, stdin);
		if (fileName[0] == '\n')         //���� ���������
		{
			return -1;
		}
		fileName[strlen(fileName) - 1] = 0;*/
		fin = fopen("13.txt", "r");
		if (fin)
		{
			printf("OK\n");
			break;
		}
		else
		{
			printf("������! (�� ���� ������� ����)\n");
		}
	}
	
	for (i = 0; i<N && fgets(strArr[i], DL, fin); i++)/*������ �� ����� � ������ strArr*/
	{
		printf("%s", strArr[i]);
		if (strArr[i][0] == '\n')
		{
			break;
		}
		kol++;
	}
	return kol;
}

int getWords(char str[], char *wordsStart[], char *wordsEnd[]) /*������� ����������� ����*/
/* *wordsStart - ��������� ������ �����, *wordsEnd - ��������� �� ����� �����*/
{
	int i = 0;/*�������*/
	int	cout = 0;/*������� ����*/
	int inWord = 0;

	for (i = 0; str[i] && str[i] != '\n'; i++)
	{
		if (inWord == 0 && str[i] != ' ')
		{
			inWord = 1;
			cout++;
			wordsStart[cout - 1] = &str[i];
		}
		//if ((*str)[i] == ' ') {inWord = 0;}
		if (inWord == 1 && str[i] == ' ')
		{
			inWord = 0;
			wordsEnd[cout - 1] = &str[i - 1];
		}
	}

	if (cout)
		wordsEnd[cout - 1] = &str[i - 1];

	return cout;
}

void changeWord(char * wordStart, char * wordEnd)/*������� �������������� ������� � �����*/
{
	int i, cout = 0, n = 0;
	char word[DL];//������ �������� ����� ������ � ��������� ������ �����
	for (i = 1; wordStart + i != wordEnd; i++)
	{
		cout++;
		word[cout - 1] = wordStart + i;
	}

	while(cout)
	{
		i = rand() % cout;
		n++;
		*(wordStart + n) = word[i];       //������ ���������� ������� �� ������� � �����
		word[i] = word[cout - 1];
		cout--;
	}
}
