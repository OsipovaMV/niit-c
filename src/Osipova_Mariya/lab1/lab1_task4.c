#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>

int main()
{
	float rost;
	char ch;

	while (1)
	{
		printf("Vvedite rost (Vvedite simvol F dlya futov,simvol D dlya duimov ,simvol S dlya santimetrov  : ");
		scanf("%f%c", &rost, &ch);
		if (ch == 'F' || ch == 'f')
		{
			printf("%.0f futov = %.0f duimov = %.2f cm\n", rost, 12*rost,12*rost*2.54);
			break;
		}

		else if (ch == 'D' || ch == 'd')
		{
			printf("%.0f duimov  = %.2f futov = %.2f cm\n", rost, rost / 12, rost*2.54);
			break;
		}

		else if (ch == 'S' || ch == 's')
		{
			printf("%.0f cm  = %.2f duimov = %.2f futov\n", rost, rost * 12, rost/2.54/12);
			break;
		}
		else
		{
			printf("data error! \n ����������� Vvedite v formate 'F', 'f', 'D', 'd','C','c' \n");
		}
	}
	return 0;
}